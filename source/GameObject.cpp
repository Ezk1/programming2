#include "GameObject.h"
#include "TextureManager.h"



GameObject::GameObject(const char* textureSheet, int x, int y, Map* map)
{
	objTexture = TextureManager::loadTexture(textureSheet);
	currentMap = map;

	srcRect.h = 32;
	srcRect.w = 32;
	srcRect.x = 0;
	srcRect.y = 0;

	xpos = x;
	ypos = y;

	//We can scale here image if needed
	dstRect.w = srcRect.w;
	dstRect.h = srcRect.h;
}


void GameObject::Update(double deltaTime)
{
	dstRect.x = xpos;
	dstRect.y = ypos;
	//xpos++;
}

void GameObject::Render()
{
	SDL_RenderCopy(Game::renderer, objTexture, &srcRect, &dstRect);
}

void GameObject::Move(Direction val, Map* map)
{
	switch (val)
	{
	case UP:
		if (Colliding(0, -32, *map)) { break; }
		ypos -= 32;
		break;
	case DOWN:
		if (Colliding(0, 32, *map)) { break; }
		ypos += 32;
		break;
	case RIGHT:
		if (Colliding(32, 0, *map)) { break; }
		xpos += 32;
		break;
	case LEFT:
		if (Colliding(-32, 0, *map)) { break; }
		xpos -= 32;
		break;
	default:
		break;
	}
}

bool GameObject::Colliding(int moveDirX, int moveDirY, Map& map)
{
	int targetX = xpos;
	int targetY = ypos;

	int oldXIndex = xpos / 32;
	int oldYIndex = ypos / 32;

	if (moveDirX == 0)
	{
		targetY += moveDirY;
	}
	else
	{
		targetX += moveDirX;
	}

	int currentIndexX = targetX / 32;
	int currentIndexY = targetY / 32;
	
	//printf("%i", map.GetMapObject(currentIndexX, currentIndexY));

	if (map.GetMapObject(currentIndexX, currentIndexY)==1)
	{
		return true;
	}

	map.SetMapObject(oldXIndex,oldYIndex, 0);
	map.SetMapObject(currentIndexX, currentIndexY, indexValue);
	return false;
}
