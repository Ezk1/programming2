#include "Map.h"
#include "TextureManager.h"

#include <iostream>

int map1[22][16] = {
	{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
	{1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
	{1,0,1,1,1,1,0,0,0,0,0,0,1,1,0,1},
	{1,0,1,0,0,0,0,0,0,0,0,0,0,1,0,1},
	{1,0,0,0,0,0,0,0,0,0,0,0,0,1,0,1},
	{1,0,0,0,0,0,0,0,0,0,0,0,0,1,0,1},
	{1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
	{1,0,0,0,0,0,1,0,0,0,0,0,0,0,0,1},
	{1,0,0,0,0,0,1,0,0,0,0,0,0,0,0,1},
	{1,0,0,0,0,0,1,1,1,1,1,1,0,0,0,1},
	{1,0,0,0,0,0,0,1,0,0,0,0,0,0,0,1},
	{1,0,0,0,0,0,0,1,0,0,0,0,0,0,0,1},
	{1,0,0,0,0,0,0,1,0,0,0,0,0,0,0,1},
	{1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
	{1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
	{1,0,1,0,0,0,0,0,0,0,0,0,0,0,0,1},
	{1,0,1,0,0,0,0,0,0,0,0,0,0,0,0,1},
	{1,0,1,0,0,0,0,4,0,0,0,0,0,0,0,1},
	{1,0,1,0,0,0,0,0,0,0,0,0,0,1,0,1},
	{1,0,1,1,0,0,0,0,0,0,1,1,1,1,0,1},
	{1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
	{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}
};

Map::Map() {
	wall = TextureManager::loadTexture("assets/wall.png");
	dot = TextureManager::loadTexture("assets/dot.png");

	LoadMap(map1);

	src.x = src.y = 0;
	src.w = dest.w = 32;
	src.h = dest.h = 32;

	dest.x = dest.y = 0;
}


//Kaipaisi dynaamista skaalausta
void Map::LoadMap(int arr[22][16]) {

	int counter = 0;

	for (int column = 0; column < 22; column++)
	{
		for (int row = 0; row < 16; row++)
		{
			map[column][row] = arr[column][row];
			if (map[column][row] == 4 )
			{
				starPosX = column;
				starPosY = row;
			}
		}
	}

	//Koon laskenta
	/*printf("%i",sizeof(map)/ sizeof(map[0]));
	printf("\n");
	printf("%i", sizeof(map[0])/ sizeof(map[0][0]));*/

	nMapWidth = sizeof(map) / sizeof(map[0]);
	nMapHeight = sizeof(map[0]) / sizeof(map[0][0]);

	pathNodes = new aNode[nMapWidth * nMapHeight];
	for (int column = 0; column < nMapWidth; column++)
	{
		for (int row = 0; row < nMapHeight; row++)
		{
			pathNodes[row * nMapWidth + column].x = column;
			pathNodes[row * nMapWidth + column].y = row;

			if (map[column][row] == 1)
			{
				pathNodes[row * nMapWidth + column].bObstacle = true;
			}
			else
			{
				pathNodes[row * nMapWidth + column].bObstacle = false;
			}

			pathNodes[row * nMapWidth + column].parent = nullptr;
			pathNodes[row * nMapWidth + column].bVisited = false;

		}
	}

	for (int column = 0; column < nMapWidth; column++)
	{
		for (int row = 0; row < nMapHeight; row++)
		{
			//Fill neighbours
			if (row > 0)
			{
				pathNodes[row * nMapWidth + column].vecNeighbours.push_back(&pathNodes[(row - 1) * nMapWidth + (column + 0)]);
			}
			if (row < nMapHeight - 1)
			{
				pathNodes[row * nMapWidth + column].vecNeighbours.push_back(&pathNodes[(row + 1) * nMapWidth + (column + 0)]);
			}
			if (column > 0)
			{
				pathNodes[row * nMapWidth + column].vecNeighbours.push_back(&pathNodes[(row + 0) * nMapWidth + (column - 1)]);
			}
			if (column < nMapWidth - 1)
			{
				pathNodes[row * nMapWidth + column].vecNeighbours.push_back(&pathNodes[(row + 0) * nMapWidth + (column + 1)]);
			}
		}
	}
}

void Map::DrawMap() {
	
	int type = 0;

	//pathNodes = aNode[]

	for (int column = 0; column < 22; column++)
	{
		for (int row = 0; row < 16; row++)
		{
			type = map[column][row];
			dest.x = column * 32;
			dest.y = row * 32;

			switch (type)
			{
			case 1:
				TextureManager::Draw(wall, src, dest);
				break;
			default:
				break;
			}
		}
	}

	////Debug A*
	//if (nodeEnd != nullptr)
	//{
	//	aNode* n = nodeEnd;
	//	while (n->parent != nullptr)
	//	{
	//		dest.x = n->x * 32;
	//		dest.y = n->y * 32;
	//		TextureManager::Draw(dot, src, dest);

	//		n = n->parent;
	//	}
	//}
}

int Map::GetMapObject(int currentIndexX, int currentIndexY)
{
	return map[currentIndexX][currentIndexY];
}

void Map::SetMapObject(int currentIndexX, int currentIndexY, int value)
{
	map[currentIndexX][currentIndexY] = value;
}

std::vector<int> Map::Solve_AStar(int startX, int startY, int targetX, int targetY)
{
	//Divide sprite size
	startX /= 32;
	startY /= 32;
	targetX /= 32;
	targetY /= 32;

	// Reset Navigation Graph - default all node states
	for (int x = 0; x < nMapWidth; x++)
		for (int y = 0; y < nMapHeight; y++)
		{
			pathNodes[y * nMapWidth + x].bVisited = false;
			pathNodes[y * nMapWidth + x].fGlobalGoal = INFINITY;
			pathNodes[y * nMapWidth + x].fLocalGoal = INFINITY;
			pathNodes[y * nMapWidth + x].parent = nullptr;	// No parents
		}

	auto distance = [](aNode* a, aNode* b) // Calculate distance for heuristic
	{
		return sqrtf((a->x - b->x) * (a->x - b->x) + (a->y - b->y) * (a->y - b->y));
	};

	auto heuristic = [distance](aNode* a, aNode* b) // So we can experiment with heuristic
	{
		return distance(a, b);
	};

	nodeStart = &pathNodes[startY * nMapWidth + startX];
	nodeEnd = &pathNodes[targetY * nMapWidth + targetX];

	// Setup starting conditions
	aNode* nodeCurrent = nodeStart;
	nodeStart->fLocalGoal = 0.0f;
	nodeStart->fGlobalGoal = heuristic(nodeStart, nodeEnd);

	// Add start node to not tested list - this will ensure it gets tested.
	// As the algorithm progresses, newly discovered nodes get added to this
	// list, and will themselves be tested later
	std::list<aNode*> listNotTestedNodes;
	listNotTestedNodes.push_back(nodeStart);

	// if the not tested list contains nodes, there may be better paths
	// which have not yet been explored. However, we will also stop 
	// searching when we reach the target - there may well be better
	// paths but this one will do - it wont be the longest.
	while (!listNotTestedNodes.empty() && nodeCurrent != nodeEnd)// Find absolutely shortest path // && nodeCurrent != nodeEnd)
	{
		// Sort Untested nodes by global goal, so lowest is first
		listNotTestedNodes.sort([](const aNode* lhs, const aNode* rhs) { return lhs->fGlobalGoal < rhs->fGlobalGoal; });

		// Front of listNotTestedNodes is potentially the lowest distance node. Our
		// list may also contain nodes that have been visited, so ditch these...
		while (!listNotTestedNodes.empty() && listNotTestedNodes.front()->bVisited)
			listNotTestedNodes.pop_front();

		// ...or abort because there are no valid nodes left to test
		if (listNotTestedNodes.empty())
			break;

		nodeCurrent = listNotTestedNodes.front();
		nodeCurrent->bVisited = true; // We only explore a node once


		// Check each of this node's neighbours...
		for (auto nodeNeighbour : nodeCurrent->vecNeighbours)
		{
			// ... and only if the neighbour is not visited and is 
			// not an obstacle, add it to NotTested List
			if (!nodeNeighbour->bVisited && nodeNeighbour->bObstacle == 0)
				listNotTestedNodes.push_back(nodeNeighbour);

			// Calculate the neighbours potential lowest parent distance
			float fPossiblyLowerGoal = nodeCurrent->fLocalGoal + distance(nodeCurrent, nodeNeighbour);

			// If choosing to path through this node is a lower distance than what 
			// the neighbour currently has set, update the neighbour to use this node
			// as the path source, and set its distance scores as necessary
			if (fPossiblyLowerGoal < nodeNeighbour->fLocalGoal)
			{
				nodeNeighbour->parent = nodeCurrent;
				nodeNeighbour->fLocalGoal = fPossiblyLowerGoal;

				// The best path length to the neighbour being tested has changed, so
				// update the neighbour's score. The heuristic is used to globally bias
				// the path algorithm, so it knows if its getting better or worse. At some
				// point the algo will realise this path is worse and abandon it, and then go
				// and search along the next best path.
				nodeNeighbour->fGlobalGoal = nodeNeighbour->fLocalGoal + heuristic(nodeNeighbour, nodeEnd);
			}
		}
	}

	//Return direction where to move
	aNode* directionNode = nodeEnd;
	while (directionNode->parent != nullptr&&directionNode->parent->fLocalGoal != 0)
	{
		directionNode = directionNode->parent;
	}
	std::vector<int> location = { directionNode->x,directionNode->y };
	return location;
}