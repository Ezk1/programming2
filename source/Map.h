#pragma once
#include "Game.h"
#include <list>

class Map
{
public:
	Map();
	~Map() {}

	void LoadMap(int arr[22][16]);
	void DrawMap();

	int GetMapObject(int currentIndexX,  int currentIndexY);
	void SetMapObject(int currentIndexX, int currentIndexY, int value);

	std::vector<int> Solve_AStar(int startX, int startY, int targetX, int targetY);

	int GetStarPosX() { return starPosX; }
	int getStarPosY() { return starPosY; }

protected:

	struct aNode
	{
		bool bObstacle = false;				// Is the node an obstruction?
		bool bVisited = false;				// Have we searched this node before?
		float fGlobalGoal = .0f;			// Distance to goal so far
		float fLocalGoal = .0f;				// Distance to goal if we took the alternative route
		int x = 0;							// Nodes position in 2D space
		int y = 0;
		std::vector<aNode*> vecNeighbours;	// Connections to neighbours
		aNode* parent = nullptr;			// Node connecting to this node that offers shortest parent
	};

	int starPosX = 0;
	int starPosY = 0;

private:
	SDL_Rect src, dest;

	SDL_Texture* wall;
	SDL_Texture* dot;

	int map[22][16];

	aNode* pathNodes = nullptr;
	aNode* nodeStart = nullptr;
	aNode* nodeEnd = nullptr;

	int nMapWidth = 0;
	int nMapHeight = 0;
};
