#pragma once
#include "Game.h"
#include "Map.h"

class GameObject
{
public:
	GameObject(const char* textureSheet, int x, int y, Map* map);
	~GameObject() {}

	enum Direction { UP, DOWN, LEFT, RIGHT, NONE };

	virtual void Update(double deltaTime = 0);
	void Render();
	void Move(Direction movedir, Map* map);

	bool Colliding(int moveDirX, int moveDirY, Map& map);

	int getX() { return xpos; }
	void setX(int value) { xpos = value; }
	int getY() { return ypos; }
	void setY(int value) { ypos = value; }

	void SetValue(int value) { indexValue = value; }
	int GetValue() { return indexValue; }

	//Only player
	void SetISCAUGHT(bool flag) { isCaught = flag; }
	bool GetISCAUGHT() { return isCaught; }

protected:
	
	int xpos = 0;
	int ypos = 0;

	int indexValue = 0; //Track position on map

	SDL_Texture* objTexture = nullptr;
	SDL_Renderer* renderer = nullptr;
	Map* currentMap = nullptr;

	SDL_Rect srcRect, dstRect;

	//For player
	bool isCaught = false;
	bool atGoal = false;
};

