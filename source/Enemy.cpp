#include "Enemy.h"

void Enemy::StateMachine()
{
		switch (currentState)
		{
		case PATROL:
			Patrolling();
			break;
		case HOSTILE:
			Chasing();
			break;
		case RETURNING:
			Returning();
			break;
		default:
			break;
		}
}
	
void Enemy::Patrolling()
{
	LookAround();

	//currentMap->Solve_AStar(xpos, ypos, player->getX(), player->getY()); <<<For debug

	switch (currentDir)
	{
	case GameObject::UP:
		if (!TryMove()) { break; }
		i++;
		if (i >= 2)
		{
			i = 0;
			currentDir = RIGHT;
		}
		break;
	case GameObject::DOWN:
		if (!TryMove()) { break; }
		i++;
		if (i >= 2)
		{
			i = 0;
			currentDir = LEFT;
		}
		break;
	case GameObject::LEFT:
		if (!TryMove()) { break; }
		i++;
		if (i >= 2)
		{
			i = 0;
			currentDir = UP;
		}
		break;
	case GameObject::RIGHT:
		if (!TryMove()) { break; }
		i++;
		if (i >= 2)
		{
			i = 0;
			currentDir = DOWN;
		}
		break;
	default:
		break;
	}
}

void Enemy::Chasing()
{
	//Get x,y for direction where to chase
	std::vector<int> direction = currentMap->Solve_AStar(xpos,ypos,player->getX(),player->getY());

	//Adjust positions according sprites
	for (auto& i : direction)
	{
		i *= 32;
	}

	if (direction[0] > xpos)
	{
		currentDir = RIGHT;
		TryMove();
	}
	else if (direction[0] < xpos)
	{
		currentDir = LEFT;
		TryMove();
	}
	else if (direction[1] < ypos)
	{
		currentDir = UP;
		TryMove();
	}
	else if (direction[1] > ypos)
	{
		currentDir = DOWN;
		TryMove();
	}
	else
	{
		player->SetISCAUGHT(true);
	}

	if (IsOutOfLeashRange())
	{
		currentState = RETURNING;
	}
}

void Enemy::Returning()
{
	LookAround();
	//Get x,y for direction where to go
	std::vector<int> direction = currentMap->Solve_AStar(xpos, ypos, lastPatrolPosX, lastPatrolPosY);

	//Adjust positions according sprites
	for (auto& i : direction)
	{
		i *= 32;
	}

	if (direction[0] > xpos)
	{
		currentDir = RIGHT;
		TryMove();
	}
	else if (direction[0] < xpos)
	{
		currentDir = LEFT;
		TryMove();
	}
	else if (direction[1] < ypos)
	{
		currentDir = UP;
		TryMove();
	}
	else if (direction[1] > ypos)
	{
		currentDir = DOWN;
		TryMove();
	}
	else
	{
		currentState = PATROL;
	}
}

void Enemy::Update(double deltaTime)
{
	dstRect.x = xpos;
	dstRect.y = ypos;

	delayTime += (float)deltaTime;

	StateMachine();
}

bool Enemy::TryMove()
{
	if (delayTime >= curGameSpeed)
	{
		delayTime = 0;
		Move(currentDir, currentMap);
		return true;
	}
	return false;
}

void Enemy::LookAround()
{
	//Take topleft point of search area
	int IndexX = xpos / 32 - searchRange;
	int IndexY = ypos / 32 - searchRange;

	for (int column = IndexX; column < IndexX + 5; column++)
	{
		for (int row = IndexY; row < IndexY + 5; row++)
		{
			if (currentMap->GetMapObject(column, row)==2)
			{
				currentState = HOSTILE;
				currentDir = NONE;
				lastPatrolPosX = xpos;
				lastPatrolPosY = ypos;
				return;
			}
		}
	}
}

bool Enemy::IsOutOfLeashRange()
{
	if (abs(xpos - lastPatrolPosX) > leashRange * 32 || abs(ypos - lastPatrolPosY) > leashRange * 32)
	{
		return true;
	}
	return false;
}
