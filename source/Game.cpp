#include "Game.h"
#include "TextureManager.h"
#include "GameObject.h"
#include "Enemy.h"
#include "Map.h"

SDL_Renderer* Game::renderer = nullptr;

std::vector<std::shared_ptr<GameObject>> vecGameObjects;
std::shared_ptr<GameObject> player = nullptr;
std::shared_ptr<GameObject> star = nullptr;

Map* map;
GameObject::Direction moveDir;

Game::Game() {}

Game::~Game() {}

void Game::init(bool fullScreen)
{
	int screenFlags = SDL_WINDOW_SHOWN;
	if (fullScreen)
	{
		screenFlags = SDL_WINDOW_FULLSCREEN;
	}

	if (SDL_Init(SDL_INIT_EVERYTHING) == 0)
	{
		window = SDL_CreateWindow("title", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 704, 512, screenFlags);
		if (window == NULL)
		{
			printf("Window failed to create. Error: %s\n", SDL_GetError());
		}

		renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
		if (renderer == NULL)
		{
			printf("Renderer failed to create. Error: %s\n", SDL_GetError());
		}
		SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);

		map = new Map();

		player = std::make_shared<GameObject>("assets/player.png", 32, 32, map);
		player->SetValue(2); //Assign indexValue 2 for player
		vecGameObjects.push_back(player);
		
		std::shared_ptr<Enemy> enemy = std::make_shared<Enemy>("assets/enemy.png", 256, 256, map, player);
		vecGameObjects.push_back(enemy);

		std::shared_ptr<Enemy> enemy2 = std::make_shared<Enemy>("assets/enemy.png", 448, 192, map, player);
		vecGameObjects.push_back(enemy2);

		star = std::make_shared<GameObject>("assets/star.png", map->GetStarPosX() * 32, map->getStarPosY() * 32, map);
		vecGameObjects.push_back(star);

	}
	else
	{
		isRunning = false;
		printf("Subsystems failed to initialize. Error: %s\n", SDL_GetError());
	}
}

void Game::handleEvents() {
	SDL_Event e;
	SDL_PollEvent(&e);

	if (e.type == SDL_QUIT || e.key.keysym.sym == SDLK_ESCAPE)
	{
		isRunning = false;
		printf("ended");
	}
	else if(e.type == SDL_KEYDOWN)
	{
		switch (e.key.keysym.sym)
		{
		case SDLK_UP:
			player->Move(GameObject::Direction::UP, map);
			break;
		case SDLK_DOWN:
			player->Move(GameObject::Direction::DOWN, map);
			break;
		case SDLK_LEFT:
			player->Move(GameObject::Direction::LEFT, map);
			break;
		case SDLK_RIGHT:
			player->Move(GameObject::Direction::RIGHT, map);
			break;
		default:
			break;
		}

		//Is player at star
		if (player->getX() == star->getX() && player->getY() == star->getY())
		{
			victory();
		}
	}

}

void Game::victory()
{
	isRunning = false;
	printf("\n");
	printf("Voitit pelin\n");
	SDL_Delay(2000);
}

void Game::defeat()
{
	isRunning = false;
	printf("\n");
	printf("H�visit pelin\n");
	SDL_Delay(2000);
}

void Game::update(double deltaTime) {
	
	for (auto& object : vecGameObjects)
	{
		object->Update(deltaTime);
	}

	//Tarkasttellaan pelaajan flagej�
	if (player->GetISCAUGHT())
	{
		defeat();
	}
}

void Game::render() {
	SDL_RenderClear(renderer);

	//Render stuff
	map->DrawMap();

	for (auto& object : vecGameObjects)
	{
		object->Render();
	}

	SDL_RenderPresent(renderer);
}



void Game::close() {
	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(window);

	vecGameObjects.erase(vecGameObjects.begin(), vecGameObjects.end());
	
	IMG_Quit();
	SDL_Quit();
}