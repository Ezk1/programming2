#include "Game.h"

Game* game = nullptr;

int main(int argc, char* args[])
{
	const int FPS = 60;
	const int frameDelay = 1000 / FPS;

	Uint32 frameStart;
	int frameTime;

	Uint64 NOW = SDL_GetPerformanceCounter();
	Uint64 LAST = 0;
	double deltaTime = 0;
	double cumulativeTime = 0;
	double cumulativeTime2 = 0;


	game = new Game();
	game->init(false);
	
	int maxI = 10;
	int i = 0;

	while (game->running())
	{
		frameStart = SDL_GetTicks();
		LAST = NOW;
		NOW = SDL_GetPerformanceCounter();

		deltaTime = ((NOW - LAST) * 1000 / (double)SDL_GetPerformanceFrequency());

		game->handleEvents();
		game->update(deltaTime*0.001f);
		game->render();

		frameTime = SDL_GetTicks() - frameStart;
		/*if (i <= maxI)
		{
			i++;
			printf("%f \n", (float)(cumulativeTime += (deltaTime*0.001f)));
			printf("%f \n", (float)(cumulativeTime2 += (frameTime * 0.001f)));

		}*/

		if (frameDelay > frameTime)
		{
			SDL_Delay(frameDelay - frameTime);
		}
	}

	game->close();

	return 0;
}