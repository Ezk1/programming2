#pragma once
#include <SDL.h>
#include <SDL_image.h>
#include <stdio.h>
#include <string>
#include <vector>
#include <memory>

class Game {

public:
	Game();
	~Game();

	void init(bool fullScreen);
	void handleEvents();
	void update(double deltaTime = 0);
	void render();

	void victory();
	void defeat();

	void close();

	static SDL_Renderer* renderer;

	bool running() { return isRunning; }

	float getGameSpeed() { return gameSpeed; }

protected:

	bool isRunning = true;

private:

	SDL_Window* window = nullptr;

	float gameSpeed = 0.7f;
	
};

