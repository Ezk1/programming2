#pragma once
#include "GameObject.h"

enum States
{
	PATROL,
	HOSTILE,
	RETURNING
};

class Enemy : public GameObject
{
public:
	Enemy(const char* textureSheet = NULL, int x = 0, int y = 0, Map* map = NULL, std::shared_ptr<GameObject> playerRef = NULL) : GameObject{ textureSheet, x, y, map }
	{
		Game* game = new Game();
		curGameSpeed = game->getGameSpeed();
		delete game;
		indexValue = 3; //Enemies are value 3

		player = playerRef;//Get ref for player pointer

	}
	~Enemy() {}
	
	//void Move();
	void StateMachine();
	void Update(double deltaTime = 0);

	bool TryMove();
	void LookAround();

	void Patrolling();
	void Chasing();
	void Returning();

	bool IsOutOfLeashRange();

	void SetGamespeed(float addValue) { curGameSpeed += addValue; }

private:
	States currentState = PATROL;

	Direction currentDir = RIGHT;

	std::shared_ptr<GameObject> player;

	int lastPatrolPosX = 0;
	int lastPatrolPosY = 0;

	int leashRange = 6;
	int searchRange = 3;

	int i = 0;
	float delayTime = 0;
	float curGameSpeed = 0;
};